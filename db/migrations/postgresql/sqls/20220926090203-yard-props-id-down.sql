ALTER TABLE yards_properties DROP CONSTRAINT "yard_properties_pk";
ALTER TABLE yards_properties ADD COLUMN "id" serial4;
ALTER TABLE yards_properties ADD CONSTRAINT "yard_properties_pk" PRIMARY KEY (id)
