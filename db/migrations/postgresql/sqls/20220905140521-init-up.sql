CREATE TABLE IF NOT EXISTS yards (
    "id" varchar(255) NOT NULL,
    "name" varchar(255),
    "geometry" geometry,
    "operating_hours" varchar(255),
    "operator" varchar(255),
    "type" varchar(255),
    "contact" varchar(255) NULL,
    "district" varchar(255) NULL,
    "address_formatted" text NULL,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT yards_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS yards_properties (
    "id" serial4 NOT NULL,
    "description" text NOT NULL,
    "type_id" varchar(50) NOT NULL,
    "value" text NOT NULL,
    "yards_id" varchar(255) NOT NULL,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT yard_properties_pk PRIMARY KEY ("id"),
    CONSTRAINT yard_properties_yards_id_fk FOREIGN KEY ("yards_id") REFERENCES yards("id") ON DELETE CASCADE
);
