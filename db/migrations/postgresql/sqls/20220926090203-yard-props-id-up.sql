ALTER TABLE yards_properties DROP CONSTRAINT "yard_properties_pk";
ALTER TABLE yards_properties DROP COLUMN "id";
ALTER TABLE yards_properties ADD CONSTRAINT "yard_properties_pk" PRIMARY KEY (yards_id, type_id);
