import { Point } from "geojson";
import { IWasteCollectionYardProperty } from "#sch/WasteCollectionYardsProperties";

export interface IWasteCollectionYard {
    id: string;
    name: string;
    operating_hours: string;
    operator: string;
    type: string | null;
    contact: string;
    geometry: Point;
    district?: string;
    address_formatted: string;
    active: boolean;
    icz: string | null;

    properties?: IWasteCollectionYardProperty[];
}

export const wasteCollectionYards = {
    name: "WasteCollectionYards",
    pgTableName: "yards",
};
