export interface IWasteCollectionYardProperty {
    description: string;
    type_id: string;
    value: string;
    yards_id: string;
}

export const wasteCollectionYardsProperties = {
    name: "WasteCollectionYardsProperties",
    pgTableName: "yards_properties",
};
