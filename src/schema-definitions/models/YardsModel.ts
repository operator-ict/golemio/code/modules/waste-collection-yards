import { IWasteCollectionYard } from "#sch/WasteCollectionYards";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";

export class YardsModel extends Model<IWasteCollectionYard> implements IWasteCollectionYard {
    declare id: string;
    declare name: string;
    declare geometry: Point;
    declare operating_hours: string;
    declare operator: string;
    declare type: string;
    declare contact: string;
    declare district?: string;
    declare address_formatted: string;
    declare active: boolean;
    declare icz: string | null;

    public static attributeModel: ModelAttributes<YardsModel, IWasteCollectionYard> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        name: DataTypes.STRING(255),
        geometry: DataTypes.GEOMETRY,
        operating_hours: DataTypes.STRING(255),
        operator: DataTypes.STRING(255),
        type: DataTypes.STRING(255),
        contact: DataTypes.STRING(255),
        district: DataTypes.STRING(255),
        address_formatted: DataTypes.STRING(255),
        active: DataTypes.BOOLEAN,
        icz: DataTypes.STRING(50),
    };

    public static updateAttributes = Object.keys(YardsModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IWasteCollectionYard>;

    public static jsonSchema: JSONSchemaType<IWasteCollectionYard[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                name: { type: "string" },
                operating_hours: { type: "string" },
                operator: { type: "string" },
                type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                contact: { type: "string" },
                district: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                geometry: { $ref: "#/definitions/geometry" },
                address_formatted: { type: "string" },
                icz: { type: "string" },
                active: { type: "boolean" },
            },
            required: ["id", "active"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
