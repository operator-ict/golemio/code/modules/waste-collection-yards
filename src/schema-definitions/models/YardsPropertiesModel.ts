import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IWasteCollectionYardProperty } from "#sch/WasteCollectionYardsProperties";

export class YardsPropertiesModel extends Model<IWasteCollectionYardProperty> implements IWasteCollectionYardProperty {
    declare description: string;
    declare type_id: string;
    declare value: string;
    declare yards_id: string;

    public static attributeModel: ModelAttributes<YardsPropertiesModel, IWasteCollectionYardProperty> = {
        yards_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        type_id: {
            type: DataTypes.STRING(50),
            primaryKey: true,
        },
        description: DataTypes.TEXT,
        value: DataTypes.TEXT,
    };

    public static updateAttributes = Object.keys(YardsPropertiesModel.attributeModel)
        .filter((att) => !["yards_id", "type_id"].includes(att))
        .concat("updated_at") as Array<keyof IWasteCollectionYardProperty>;

    public static jsonSchema: JSONSchemaType<IWasteCollectionYardProperty[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                yards_id: { type: "string" },
                type_id: { type: "string" },
                description: { type: "string" },
                value: { type: "string" },
            },
            required: ["description", "type_id", "value", "yards_id"],
        },
    };
}
