import { wasteCollectionYardsDatasource } from "#sch/datasources/WasteCollectionYardsJsonSchema";
import { wasteCollectionYards } from "#sch/WasteCollectionYards";
import { wasteCollectionYardsProperties } from "#sch/WasteCollectionYardsProperties";

const forExport: any = {
    name: "WasteCollectionYards",
    pgSchema: "waste_collection_yards",
    datasources: {
        wasteCollectionYardsDatasource,
    },
    definitions: {
        wasteCollectionYards,
        wasteCollectionYardsProperties,
    },
};

export { forExport as WasteCollectionYards };
