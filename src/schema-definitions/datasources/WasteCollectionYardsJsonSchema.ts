import { Point } from "geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";

export interface IWasteCollectionYardsFeature {
    type: string;
    id: number;
    geometry: Point;
    properties: {
        ADRESA: string;
        KONTAKT: string;
        NAZEV: string;
        NEBEZPODPADPRIJEM?: string;
        OBJECTID: number;
        ODPADOMEZENI?: string;
        ODPADPRIJEM?: string;
        PLATBANEBEZPODPAD?: string;
        PLATBAODPAD?: string;
        POZNAMKA?: string;
        PROVOZNIDOBA: string;
        PROVOZOVATEL: string;
        ZPETNYODBER?: string;
        PLATNOST?: number;

        REUSE: number; // contains 0 or 1
        REUSE_PRIJEM: string;
        REUSE_PROVOZDOBA: string;
        REUSE_INFO: string;
        ICZ: string | null;
    };
}

const wasteCollectionYardsJsonSchema: JSONSchemaType<IWasteCollectionYardsFeature[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            geometry: { $ref: "#/definitions/geometry" },
            properties: {
                type: "object",
                properties: {
                    ADRESA: { type: "string" },
                    KONTAKT: { type: "string" },
                    NAZEV: { type: "string" },
                    NEBEZPODPADPRIJEM: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    OBJECTID: { type: "integer" },
                    ODPADOMEZENI: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    ODPADPRIJEM: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    PLATBANEBEZPODPAD: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    PLATBAODPAD: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    POZNAMKA: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    PROVOZNIDOBA: { type: "string" },
                    PROVOZOVATEL: { type: "string" },
                    ZPETNYODBER: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    PLATNOST: { type: "integer" },
                },
                required: ["ADRESA", "KONTAKT", "NAZEV", "OBJECTID", "PROVOZNIDOBA", "PROVOZOVATEL", "PLATNOST"],
                additionalProperties: true,
            },
            type: { type: "string" },
            id: { type: "number" },
        },
        required: ["geometry", "properties", "type"],
        additionalProperties: false,
    },
    definitions: {
        // @ts-expect-error
        geometry: SharedSchemaProvider.Geometry,
    },
};

export const wasteCollectionYardsDatasource: { name: string; jsonSchema: JSONSchemaType<IWasteCollectionYardsFeature[]> } = {
    name: "WasteCollectionYardsDatasource",
    jsonSchema: wasteCollectionYardsJsonSchema,
};
