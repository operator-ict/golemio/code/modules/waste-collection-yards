import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WasteCollectionYards } from "#sch";
import { YardsModel } from "#sch/models/YardsModel";
import { WasteCollectionYardsPropertiesRepository } from "#ie/repositories";
import { IWasteCollectionYardTransformation } from "#ie";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { YardsPropertiesModel } from "#sch/models/YardsPropertiesModel";

export class WasteCollectionYardsRepository extends PostgresModel implements IModel {
    public propertiesModel: WasteCollectionYardsPropertiesRepository;

    constructor() {
        super(
            WasteCollectionYards.definitions.wasteCollectionYards.name + "Repository",
            {
                outputSequelizeAttributes: YardsModel.attributeModel,
                pgTableName: WasteCollectionYards.definitions.wasteCollectionYards.pgTableName,
                pgSchema: WasteCollectionYards.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollectionYards.definitions.wasteCollectionYards.name + "Validator",
                YardsModel.jsonSchema
            )
        );

        this.propertiesModel = new WasteCollectionYardsPropertiesRepository();

        this.sequelizeModel.hasMany(this.propertiesModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "yards_id",
        });

        this.propertiesModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            targetKey: "id",
            foreignKey: "yards_id",
        });
    }

    public async updateYards(transformedData: IWasteCollectionYardTransformation) {
        const updatedAt = new Date().toISOString();
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            await this.validate(transformedData.yards);
            await this.propertiesModel.validate(transformedData.properties);

            await this.sequelizeModel.update<YardsModel>({ active: false }, { where: { active: true }, transaction: t });

            await this.sequelizeModel.bulkCreate<YardsModel>(transformedData.yards, {
                updateOnDuplicate: YardsModel.updateAttributes,
                transaction: t,
            });

            await this.propertiesModel["sequelizeModel"].bulkCreate<YardsPropertiesModel>(transformedData.properties, {
                updateOnDuplicate: YardsPropertiesModel.updateAttributes,
                transaction: t,
            });

            const yardIds = transformedData.yards.map((yard) => yard.id);
            await this.propertiesModel["sequelizeModel"].destroy({
                where: { yards_id: { [Op.in]: yardIds }, updated_at: { [Op.lt]: updatedAt } },
                transaction: t,
            });

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }
}
