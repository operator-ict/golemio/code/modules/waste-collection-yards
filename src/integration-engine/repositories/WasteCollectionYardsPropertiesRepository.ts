import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WasteCollectionYards } from "#sch";
import { YardsPropertiesModel } from "#sch/models/YardsPropertiesModel";

export class WasteCollectionYardsPropertiesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollectionYards.definitions.wasteCollectionYardsProperties.name + "Repository",
            {
                outputSequelizeAttributes: YardsPropertiesModel.attributeModel,
                pgTableName: WasteCollectionYards.definitions.wasteCollectionYardsProperties.pgTableName,
                pgSchema: WasteCollectionYards.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollectionYards.definitions.wasteCollectionYardsProperties.name + "Validator",
                YardsPropertiesModel.jsonSchema
            )
        );
    }
}
