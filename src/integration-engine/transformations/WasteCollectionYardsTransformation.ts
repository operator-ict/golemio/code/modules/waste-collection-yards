import slug from "slugify";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { WasteCollectionYards } from "#sch";
import { IWasteCollectionYard } from "#sch/WasteCollectionYards";
import { IWasteCollectionYardsFeature } from "#sch/datasources/WasteCollectionYardsJsonSchema";
import { IWasteCollectionYardProperty } from "#sch/WasteCollectionYardsProperties";

export interface IWasteCollectionYardTransformation {
    yards: IWasteCollectionYard[];
    properties: Array<Omit<IWasteCollectionYardProperty, "id">>;
}

export class WasteCollectionYardsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WasteCollectionYards.name;
    }

    public transform = async (data: IWasteCollectionYardsFeature[]): Promise<IWasteCollectionYardTransformation> => {
        const result: IWasteCollectionYardTransformation = {
            yards: [],
            properties: [],
        };

        for (const item of data) {
            if (item.properties.PLATNOST === 0) {
                continue;
            }
            const transformedData = this.transformElement(item);
            result.yards.push(...transformedData.yards);
            result.properties.push(...transformedData.properties);
        }

        return result;
    };

    protected transformElement = (element: IWasteCollectionYardsFeature): IWasteCollectionYardTransformation => {
        const id = slug(`${element.properties.NAZEV} ${element.properties.OBJECTID}`, { lower: true, remove: /[*+~.,()'"!:@]/g });

        const result: IWasteCollectionYardTransformation = {
            yards: [
                {
                    geometry: element.geometry,
                    address_formatted: element.properties.ADRESA,
                    contact: element.properties.KONTAKT,
                    id,
                    name: element.properties.NAZEV,
                    operating_hours: element.properties.PROVOZNIDOBA,
                    operator: element.properties.PROVOZOVATEL,
                    type: null,
                    active: true,
                    icz: element.properties.ICZ,
                },
            ],
            properties: [],
        };

        if (element.properties.ODPADPRIJEM) {
            result.properties.push({
                description: "Příjem odpadu",
                type_id: "ODPADPRIJEM",
                value: element.properties.ODPADPRIJEM,
                yards_id: id,
            });
        }
        if (element.properties.ODPADOMEZENI) {
            result.properties.push({
                description: "Omezení příjmu odpadu",
                type_id: "ODPADOMEZENI",
                value: element.properties.ODPADOMEZENI,
                yards_id: id,
            });
        }
        if (element.properties.NEBEZPODPADPRIJEM) {
            result.properties.push({
                description: "Příjem nebezpečného odpadu",
                type_id: "NEBEZPODPADPRIJEM",
                value: element.properties.NEBEZPODPADPRIJEM,
                yards_id: id,
            });
        }
        if (element.properties.ZPETNYODBER) {
            result.properties.push({
                description: "Zpětný odběr odpadu",
                type_id: "ZPETNYODBER",
                value: element.properties.ZPETNYODBER,
                yards_id: id,
            });
        }
        if (element.properties.PLATBAODPAD) {
            result.properties.push({
                description: "Platba za odpad",
                type_id: "PLATBAODPAD",
                value: element.properties.PLATBAODPAD,
                yards_id: id,
            });
        }
        if (element.properties.PLATBANEBEZPODPAD) {
            result.properties.push({
                description: "Platba za nebezpečný odpad",
                type_id: "PLATBANEBEZPODPAD",
                value: element.properties.PLATBANEBEZPODPAD,
                yards_id: id,
            });
        }
        if (element.properties.POZNAMKA) {
            result.properties.push({
                description: "Poznámka",
                type_id: "POZNAMKA",
                value: element.properties.POZNAMKA,
                yards_id: id,
            });
        }
        if (element.properties.REUSE === 1) {
            result.properties.push({
                description: "Re-use point",
                type_id: "REUSE",
                // eslint-disable-next-line max-len
                value: `${element.properties.REUSE_PRIJEM} ${element.properties.REUSE_PROVOZDOBA} ${element.properties.REUSE_INFO}`,
                yards_id: id,
            });
        }

        return result;
    };
}
