import {
    DataSource,
    JSONDataTypeStrategy,
    PaginatedHTTPProtocolStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { config } from "@golemio/core/dist/integration-engine/config";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WasteCollectionYards } from "#sch";

export class WasteCollectionYardsDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            WasteCollectionYards.datasources.wasteCollectionYardsDatasource.name,
            new PaginatedHTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.WasteCollectionYards,
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                WasteCollectionYards.datasources.wasteCollectionYardsDatasource.name + "Validator",
                WasteCollectionYards.datasources.wasteCollectionYardsDatasource.jsonSchema
            )
        );
    }
}
