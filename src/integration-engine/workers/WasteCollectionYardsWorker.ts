import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask, UpdateDistrictsTask } from "#ie/workers/tasks";

export class WasteCollectionYardsWorker extends AbstractWorker {
    protected readonly name = "WasteCollectionYards";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateDistrictsTask(this.getQueuePrefix()));
    }
}
