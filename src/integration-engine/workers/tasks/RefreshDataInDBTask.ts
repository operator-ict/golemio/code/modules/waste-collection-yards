import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { WasteCollectionYardsTransformation } from "#ie";
import { WasteCollectionYardsDataSourceFactory } from "#ie/datasources/WasteCollectionYardsDataSource";
import { WasteCollectionYardsRepository } from "#ie/repositories";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes

    private dataSource: DataSource;
    private transformation: WasteCollectionYardsTransformation;
    private repository: WasteCollectionYardsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = WasteCollectionYardsDataSourceFactory.getDataSource();
        this.transformation = new WasteCollectionYardsTransformation();
        this.repository = new WasteCollectionYardsRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.repository.updateYards(transformedData);

        for (const item of transformedData.yards) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateDistrict", {
                id: item.id,
            });
        }
    }
}
