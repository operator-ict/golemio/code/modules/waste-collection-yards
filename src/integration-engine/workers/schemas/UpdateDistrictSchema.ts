import { IsString } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateDistrictInput {
    id: string;
}

export class UpdateDistrictValidationSchema implements IUpdateDistrictInput {
    @IsString()
    id!: string;
}
