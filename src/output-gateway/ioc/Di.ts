import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";

//#region Initialization
const wasteCollectionYardsContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

//#region Models
//#endregion

export { wasteCollectionYardsContainer as WasteCollectionYardsContainer };
