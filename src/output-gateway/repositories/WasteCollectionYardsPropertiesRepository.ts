import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollectionYards } from "#sch";
import { YardsPropertiesModel } from "#sch/models/YardsPropertiesModel";

export class WasteCollectionYardsPropertiesRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollectionYards.definitions.wasteCollectionYardsProperties.name + "Repository",
            WasteCollectionYards.definitions.wasteCollectionYardsProperties.pgTableName,
            YardsPropertiesModel.attributeModel,
            { schema: WasteCollectionYards.pgSchema }
        );
    }

    public GetAll = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
