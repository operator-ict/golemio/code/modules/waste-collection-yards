import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IPropertyResponseModel, SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { WasteCollectionYards } from "#sch";
import { YardsModel } from "#sch/models/YardsModel";
import { WasteCollectionYardsPropertiesRepository } from "#og/repositories/WasteCollectionYardsPropertiesRepository";

interface IWasteCollectionYardPropertyOutput extends IPropertyResponseModel {
    id: string;
    title: string;
    value: string;
}

export class WasteCollectionYardsRepository extends SequelizeModel implements IGeoJsonModel {
    private readonly propertiesModel: WasteCollectionYardsPropertiesRepository;

    constructor() {
        super(
            WasteCollectionYards.definitions.wasteCollectionYards.name + "Repository",
            WasteCollectionYards.definitions.wasteCollectionYards.pgTableName,
            YardsModel.attributeModel,
            { schema: WasteCollectionYards.pgSchema }
        );

        this.propertiesModel = new WasteCollectionYardsPropertiesRepository();

        this.sequelizeModel.hasMany(this.propertiesModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "yards_id",
            as: "properties",
        });

        this.propertiesModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            targetKey: "id",
            foreignKey: "yards_id",
        });
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<IWasteCollectionYardPropertyOutput[]> => {
        return await this.propertiesModel["sequelizeModel"].findAll({
            attributes: [["type_id", "id"], ["description", "title"], "value"],
            include: [
                {
                    model: this.sequelizeModel,
                    attributes: [],
                    where: {
                        active: true,
                    },
                },
            ],
        });
    };

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection> => {
        const whereAttributes: Sequelize.WhereOptions = {
            active: true,
        };

        const order: Sequelize.Order = [];

        if (options.districts && options.districts.length > 0) {
            whereAttributes.district = {
                [Sequelize.Op.in]: options.districts,
            };
        }

        if (options.updatedSince) {
            whereAttributes.updated_at = {
                [Sequelize.Op.gt]: options?.updatedSince,
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("geometry"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geometry"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        const result = await this.sequelizeModel.findAll<YardsModel>({
            attributes: {
                include: ["updated_at"],
                exclude: ["active"],
            },
            include: [
                {
                    model: this.propertiesModel["sequelizeModel"],
                    as: "properties",
                    attributes: [["type_id", "id"], "description", "value"],
                },
            ],
            where: whereAttributes,
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });

        return buildGeojsonFeatureCollection(
            result.map((record: YardsModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<YardsModel>({
            attributes: {
                include: ["updated_at"],
                exclude: ["active"],
            },
            where: { id, active: true },
            include: [
                {
                    model: this.propertiesModel["sequelizeModel"],
                    as: "properties",
                    attributes: [["type_id", "id"], "description", "value"],
                },
            ],
        });

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: YardsModel): IGeoJSONFeature | undefined => {
        const { geometry, address_formatted, ...rest } = record.get({ plain: true });
        return buildGeojsonFeatureLatLng(
            { ...rest, ...{ address: { address_formatted } } },
            geometry.coordinates[0],
            geometry.coordinates[1]
        );
    };
}
