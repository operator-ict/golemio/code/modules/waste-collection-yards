import { WasteCollectionYardsContainer } from "#og/ioc/Di";
import { CacheHeaderMiddleware, checkErrors } from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { WasteCollectionYardsRepository } from ".";

export class WasteCollectionYardsRouter extends GeoJsonRouter {
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super(new WasteCollectionYardsRepository());
        this.cacheHeaderMiddleware = WasteCollectionYardsContainer.resolve<CacheHeaderMiddleware>(
            ContainerToken.CacheHeaderMiddleware
        );
        this.initRoutes({ maxAge: 12 * 60 * 60, staleWhileRevalidate: 60 });
        this.router.get(
            "/properties",
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.GetProperties
        );
    }

    public GetProperties = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await (this.model as WasteCollectionYardsRepository).GetProperties();
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const wasteCollectionYardsRouter: Router = new WasteCollectionYardsRouter().router;

export { wasteCollectionYardsRouter };
