# Implementační dokumentace modulu *waste-collection-yards*

## Záměr

Modul slouží k ukládání a poskytování informací o sběrných dvorech.


## Vstupní data

**Dostupní poskytovatelé**:

-   opendata IPR ArcGIS Hub

### Data aktivně stahujeme

#### *open data IPR*

- zdroj dat
  - url: [config.datasources.WasteCollectionYards](https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/FSB_CUR_BEZ_OBJEKTMPP_B/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson&resultRecordCount=1000&resultOffset=0)
- formát dat
  - protokol: http
  - datový typ: json
  - validační schéma: [wasteCollectionYardsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/blob/development/src/integration-engine/datasources/WasteCollectionYardsDataSource.ts)
  - příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/blob/development/test/integration-engine/data/wastecollectionyards-datasource.json)
- frekvence stahování
  - cron definice:
    - cron.dataplatform.wastecollectionyards.refreshDataInDB
      - rabin `0 31 7 * * *`
      - prod `0 25 2 1 * *`
- název rabbitmq fronty
  - dataplatform.wastecollectionyards.refreshDataInDB



## Zpracování dat / transformace

Při transformaci data obohacujeme o atribut `district`, která vychází z polohy daného sběrného dvoru a získává se metodou `cityDistrictsModel.getDistrict` v modulu city-district.

### *WasteCollectionYardsWorker*

#### *task: RefreshDataInDBTask*

- vstupní rabbitmq fronta
  - název: dataplatform.wastecollectionyards.refreshDataInDB
  - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název: dataplatform.wastecollectionyards.updateDistrict
  - parametry: `{ id: yards.id }`
- datové zdroje
  - dataSource IPR
- transformace
  - [WasteCollectionYardsTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/blob/development/src/integration-engine/transformations/WasteCollectionYardsTransformation.ts) - mapování pro `YardsModel` a `YardsPropertiesModel`
- obohacení dat
  - viď `UpdateDistrictsTask`
- data modely
  - yardsModel -> (schéma waste_collection_yards) `yards`
  - YardsPropertiesModel -> (schéma waste_collection_yards) `yards_properties`

#### *task: UpdateDistrictsTask*

- vstupní rabbitmq fronta
    - název: dataplatform.wastecollectionyards.updateDistrict
    - parametry: `{ id: yards.id }`
- datové zdroje
    - `city-districts` module
- data modely
    - yardsModel -> (schéma waste_collection_yards) `yards.district`

## Uložení dat

- typ databáze
    - PSQL
- databázové schéma
    - ![wastecollectionyards er diagram](./assets/wastecollectionyards_erd.png)
- retence dat
    - `yardsModel`, `YardsPropertiesModel` update

## Output API

`WasteCollectionYardsRouter` implementuje `GeoJsonRouter`.

### Obecné

- OpenAPI v3 dokumentace
  - [OpenAPI](./openapi.yaml)
- api je veřejné
- postman kolekce
  - TBD

#### _/wastecollectionyards_

- zdrojové tabulky
    - `yards`
- dodatečná transformace: Feature
-
#### _/wastecollectionyards/:id_

- zdrojové tabulky
  - `yards`
- dodatečná transformace: Feature collection

#### _/wastecollectionyards/properties_

- zdrojové tabulky
  - `yards_properties`
