import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";
import { WasteCollectionYardsRepository } from "#og";
import { IWasteCollectionYard } from "#sch/WasteCollectionYards";

chai.use(chaiAsPromised);

describe("WasteCollectionYardsRepository", () => {
    let sandbox: SinonSandbox;
    let wasteCollectionYardsRepository: WasteCollectionYardsRepository;

    before(() => {
        sandbox = createSandbox();
        wasteCollectionYardsRepository = new WasteCollectionYardsRepository();
    });

    after(() => {
        sandbox.restore();
    });

    it("should instantiate", () => {
        expect(wasteCollectionYardsRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await wasteCollectionYardsRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(2);
    });

    it("should return single item", async () => {
        const id = "stabilni-sberna-nebezpecnych-odpadu-areal-spol-mikapa-plus";
        const result = await wasteCollectionYardsRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result!.properties).to.have.property("id", id);
        expect(result!.properties).to.have.property("properties");
        expect((result!.properties as IWasteCollectionYard).properties).to.be.an.instanceof(Array);
        expect((result!.properties as IWasteCollectionYard).properties).to.have.length(2);
    });

    it("should return array of properties", async () => {
        const result = await wasteCollectionYardsRepository.GetProperties();
        expect(result).not.to.be.empty;
        expect(result).to.be.an.instanceof(Array);
        expect(result).to.have.length(3);
    });
});
