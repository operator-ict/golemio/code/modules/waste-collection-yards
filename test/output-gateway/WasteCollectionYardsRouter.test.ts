import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { wasteCollectionYardsRouter } from "#og/WasteCollectionYardsRouter";
import { outputDataFixture } from "../integration-engine/data/wastecollectionyards-output";

chai.use(chaiAsPromised);

describe("WasteCollectionYards Router", () => {
    const app = express();

    before(async () => {
        app.use("/wastecollectionyards", wasteCollectionYardsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /wastecollectionyards", (done) => {
        request(app)
            .get("/wastecollectionyards")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionyards/:id", (done) => {
        request(app)
            .get("/wastecollectionyards/stabilni-sberna-nebezpecnych-odpadu-areal-spol-mikapa-plus")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.equal(outputDataFixture);
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionyards/properties", (done) => {
        request(app)
            .get("/wastecollectionyards/properties")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });
});
