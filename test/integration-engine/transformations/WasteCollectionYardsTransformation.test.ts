import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { WasteCollectionYardsTransformation } from "#ie/transformations/WasteCollectionYardsTransformation";
import { transformedDataFixture } from "../data/wastecollectionyards-transformation";
import { IWasteCollectionYardsFeature } from "#sch/datasources/WasteCollectionYardsJsonSchema";

chai.use(chaiAsPromised);

describe("WasteCollectionYardsTransformation", () => {
    let transformation: WasteCollectionYardsTransformation;
    let testSourceData: IWasteCollectionYardsFeature[];

    beforeEach(async () => {
        transformation = new WasteCollectionYardsTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/wastecollectionyards-datasource.json", "utf8")).features;
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("WasteCollectionYards");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (const item of data.yards) {
            expect(Object.keys(item).length).to.equal(10);
            expect(item).to.have.property("geometry");
            expect(item).to.have.property("type");
            expect(item).to.have.property("id");
            expect(item).to.have.property("address_formatted");
            expect(item).to.have.property("contact");
            expect(item).to.have.property("name");
            expect(item).to.have.property("operating_hours");
            expect(item).to.have.property("operator");
            expect(item).to.have.property("active");
            expect(item).to.have.property("icz");
        }

        for (const property of data.properties) {
            expect(Object.keys(property).length).to.equal(4);
            expect(property).to.have.property("description");
            expect(property).to.have.property("type_id");
            expect(property).to.have.property("value");
            expect(property).to.have.property("yards_id");
        }
    });
});
