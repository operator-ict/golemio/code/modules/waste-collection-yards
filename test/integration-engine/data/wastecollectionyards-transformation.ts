/* eslint-disable max-len */
export const transformedDataFixture = {
    yards: [
        {
            geometry: {
                type: "Point",
                coordinates: [14.4833584560001, 50.155118332],
            },
            address_formatted: "Ďáblická 791/89, 182 00 Praha 8 (areál skládky Ďáblice)",
            contact: "720 984 402",
            id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
            name: "Sběrný dvůr hl. m. Prahy – areál skládky Ďáblice",
            operating_hours:
                "Po - Pá 8:30 - 18:00 hod. (8:30 - 17:00 hod. v zimním období), So 8:30 - 15:00 hod, občané mají přednostní právo odevzdat odpady před právnickými subjekty",
            operator: "FCC Česká republika s.r.o",
            type: null,
            active: true,
            icz: "CZA01674",
        },
    ],
    properties: [
        {
            description: "Příjem odpadu",
            type_id: "ODPADPRIJEM",
            value: "objemný odpad (nábytek, zařízení domácnosti), kovy, dřevo, odpad z údržby zeleně, suť z bytových úprav, papír, sklo, plasty, nápojové kartony, nebezpečné odpady, pneumatiky, použitý textil, obuv a oděvy, jedlé oleje a tuky (v uzavřených pet lahvích)",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Omezení příjmu odpadu",
            type_id: "ODPADOMEZENI",
            value: "omezení jednorázového návozu – možný vjezd vozidel max.do 3,5 t; suť z bytových úprav do 14 t (12 m3)/osoba/rok zdarma",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Příjem nebezpečného odpadu",
            type_id: "NEBEZPODPADPRIJEM",
            value: "rozpouštědla, kyseliny, zásady ,fotochemikálie ,pesticidy, odpad s obsahem rtuti, olej a tuk kromě jedlého, barvy, lepidla, pryskyřice, detergenty obsahující nebezp.látky (čist.prostředky), nepoužitelná léčiva",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Zpětný odběr odpadu",
            type_id: "ZPETNYODBER",
            value: "elektrická a elektronická zařízení (např. lednice, pračky, sporáky, malé spotřebiče, TV, PC, rádia, zářivky, výbojky, nástroje, hračky), zářivky a baterie",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Platba za odpad",
            type_id: "PLATBAODPAD",
            value: "zdarma pro občany s trvalým pobytem na území hl.m. Prahy; vždy je nutno se prokázat platným občanským průkazem",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Platba za nebezpečný odpad",
            type_id: "PLATBANEBEZPODPAD",
            value: "zdarma pro občany s trvalým pobytem na území hl.m. Prahy; vždy je nutno se prokázat platným občanským průkazem",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Poznámka",
            type_id: "POZNAMKA",
            value: "ve stabilních sběrnách nebezpečných odpadů zřízených v rámci Sběrných dvorů hl. m. Prahy jsou zářivky a výbojky odebírány v rámci zpětného odběru",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
        {
            description: "Re-use point",
            type_id: "REUSE",
            value: "Přijímány jsou předměty ve skvělém stavu, které dále využije někdo jiný, typově předměty z domácnosti, drobnější nábytek, knihy, hračky, sportovní vybavení atp. Provozní doba Re-use pointů se shoduje s provozní dobou sběrného dvora, na níž je point umístěn. Předměty z Re-use pointů lze vyzvednout pouze po rezervaci v aplikaci nevyhazujto.cz",
            yards_id: "sberny-dvur-hl-m-prahy-–-areal-skladky-dablice-7174",
        },
    ],
};
/* eslint-enable max-len */
