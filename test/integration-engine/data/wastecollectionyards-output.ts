export const outputDataFixture = {
    geometry: {
        coordinates: [14.550761615000056, 50.05775610200004],
        type: "Point",
    },
    properties: {
        id: "stabilni-sberna-nebezpecnych-odpadu-areal-spol-mikapa-plus",
        name: "Stabilní sběrna nebezpečných odpadů - Areál spol. MIKAPA plus",
        operating_hours: "Od ledna 2016: Po–Ne: 6:00 – 18:00 hod.",
        operator: "Mikapa plus",
        type: "SSNO",
        contact: "272705071",
        icz: null,
        district: "praha-10",
        updated_at: "2022-09-08T06:40:05.102Z",
        properties: [
            {
                id: "NEBEZPODPADPRIJEM",
                description: "Příjem nebezpečného odpadu",
                // eslint-disable-next-line max-len
                value: "rozpouštědla,kyseliny,zásady,fotochemikálie,pesticidy,zářivky a jiný odp. s obsahem rtuti,olej a tuk kromě jedlého,barvy,lepidla,pryskyřice,detergenty obsahující nebezp.látky (čist.prostředky),nepoužitelná léčiva,baterie,akumulátory",
            },
            {
                id: "PLATBANEBEZPODPAD",
                description: "Platba za nebezpečný odpad",
                // eslint-disable-next-line max-len
                value: "zdarma pro občany s trvalým pobytem na území hl.m. Prahy; vždy je nutno se prokázat platným občanským průkazem",
            },
        ],
        address: {
            address_formatted: "Dolnoměcholupská 28, Praha 10 (vedle areálu Kovošrotu - Dolnoměcholupská 27)",
        },
    },
    type: "Feature",
};
