import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { WasteCollectionYardsWorker } from "#ie/workers/WasteCollectionYardsWorker";

describe("WasteCollectionYardsWorker", () => {
    let sandbox: SinonSandbox;
    let worker: WasteCollectionYardsWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(config, "datasources").value({
            WasteCollectionYards: "http://waste-collection-yards.cz/",
        });

        worker = new WasteCollectionYardsWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".wastecollectionyards");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("WasteCollectionYards");
            expect(result.queues.length).to.equal(2);
        });
    });

    describe("registerTask", () => {
        it("should have two tasks registered", () => {
            expect(worker["queues"].length).to.equal(2);

            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][1].name).to.equal("updateDistrict");

            expect(worker["queues"][0].options.messageTtl).to.equal(59 * 60 * 1000);
            expect(worker["queues"][1].options.messageTtl).to.equal(59 * 60 * 1000);
        });
    });
});
