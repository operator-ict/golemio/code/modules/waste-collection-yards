import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { config, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { IWasteCollectionYardTransformation } from "#ie";
import { RefreshDataInDBTask } from "#ie/workers/tasks";
import { IWasteCollectionYard } from "#sch/WasteCollectionYards";
import { IWasteCollectionYardsFeature } from "#sch/datasources/WasteCollectionYardsJsonSchema";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: IWasteCollectionYardsFeature[];
    let testTransformedData: IWasteCollectionYardTransformation;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );

        sandbox.stub(config, "datasources").value({
            WasteCollectionYards: "http://waste-collection-yards.cz/",
        });

        task = new RefreshDataInDBTask("test.wastecollectionyards");

        testData = [
            {
                geometry: {
                    type: "Point",
                    coordinates: [14.553641626000058, 50.14545956000006],
                },
                properties: {
                    NAZEV: "Sběrný dvůr hlavního města Prahy Jilemnická",
                },
            },
        ] as IWasteCollectionYardsFeature[];
        testTransformedData = {
            yards: [{ id: "sberny-dvur", geometry: { coordinates: [0, 0], type: "Point" } } as IWasteCollectionYard],
            properties: [],
        };

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));

        sandbox.stub(task["repository"], "updateYards");
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].updateYards as SinonSpy);
        sandbox.assert.calledWith(task["repository"].updateYards as SinonSpy, testTransformedData);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 1);

        for (const data of testTransformedData.yards) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.wastecollectionyards",
                "updateDistrict",
                { id: "sberny-dvur" }
            );
        }

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].updateYards as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
