import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { UpdateDistrictsTask } from "#ie/workers/tasks";
import { IUpdateDistrictInput } from "#ie/workers/schemas/UpdateDistrictSchema";
import { YardsModel } from "#sch/models/YardsModel";

describe("UpdateDistrictTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateDistrictsTask;
    let msg: IUpdateDistrictInput;
    let yardsModel: YardsModel;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        msg = { id: "sberny-dvur" };
        yardsModel = {
            id: "sberny-dvur",
            geometry: { coordinates: [0, 0], type: "Point" },
            district: undefined,
        } as unknown as YardsModel;

        task = new UpdateDistrictsTask("test.wastecollectionyards");

        sandbox.stub(task["repository"], "findOne").callsFake(() => Promise.resolve(yardsModel));
        sandbox.stub(task["repository"], "save");

        sandbox.stub(task["cityDistrictsModel"], "getDistrict").callsFake(() => Promise.resolve("praha-22"));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by updateDistrict method", async () => {
        await task["execute"]({ id: "sberny-dvur" });
        sandbox.assert.calledOnce(task["repository"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["repository"].findOne as SinonSpy, { where: { id: msg.id }, raw: true });

        sandbox.assert.calledOnce(task["cityDistrictsModel"].getDistrict as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].save as SinonSpy);
        sandbox.assert.calledWith(task["repository"].save as SinonSpy, [{ ...yardsModel, district: "praha-22" }]);

        sandbox.assert.callOrder(
            task["repository"].findOne as SinonSpy,
            task["cityDistrictsModel"].getDistrict as SinonSpy,
            task["repository"].save as SinonSpy
        );
    });
});
