# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.16] - 2024-10-24

### Fixed

-   add prefixes to asyncAPI

## [1.2.15] - 2024-10-22

### Added

-   asyncAPI docs file ([integration-engine#260](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/260))

## [1.2.14] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.13] - 2024-07-17

### Fixed

-   API validations ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.2.12] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.11] - 2024-04-29

### Added

-   add cache-control header to all responses ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

### Removed

-   remove redis useCacheMiddleware ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

## [1.2.10] - 2024-04-05

### Fixed

-   API docs inconsistencies (https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/merge_requests/59)

### Changed

-   device_id to icz ([p0149#343](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/343))

## [1.2.9] - 2024-04-03

### Added

-   new entities processing from datasource `ICZ,REUSE,REUSE_PRIJEM,REUSE_PROVOZDOBA,REUSE_INFO` ([p0149#343](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/343))

## [1.2.8] - 2024-01-29

### Fixed

-   IPR ArcGIS datasource ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

## [1.2.7] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.6] - 2023-07-10

### Added

-   `active` column property for filtering out closed waste collection yards on API [#4](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/issues/4)

## [1.2.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.4] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.3] - 2023-05-29

### Changed

-   Update openapi docs

## [1.2.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.1] - 2023-02-22

### Changed

-   Update city-districts

## [1.2.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.1.1] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.1.0] - 2022-10-11

### Fixed

-   range query parameter and properties fix [#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/issues/3)

## [1.0.4] - 2022-09-21

### Changed

-   mongo to postgresql migration [#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection-yards/-/issues/3)

## [1.0.3] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.2] - 2022-03-01

### Added

-   openapi: 3.0.3 doc
